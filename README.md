Spring4D
========
Spring4D is an open-source code library for Embarcadero Delphi 2010 and higher.
It consists of a number of of different modules that contain a base class library (common types, interface based collection types, reflection extensions) and a dependency injection framework. It uses the Apache License 2.0.

Installation
------------
Just run the Build.exe and select the Delphi versions you want to install Spring4D for.

Current version
---------------
1.0 (2014-04-01)

Known issues
------------
Some warnings when compiling for mobile compilers.
The deployment of the unit test project might fail for mobile compilers (iOS ARM and Android).

Copyright (c) 2009 - 2014 Spring4D Team